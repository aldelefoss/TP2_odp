package fr.isima.myant;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

public class Project {
	private String name;
	private List<Target> targets;

	public List<Target> getTargets() {
		return targets;
	}

	public void setTargets(List<Target> targets) {
		this.targets = targets;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void init() {
		Target t = new Target("default");
		t.addTask(new EchoTask("message"));
		
		this.addTarget(t);
	}
	
	public void loadFromFile() {
        String fileName = "mybuild.txt";
        String line = null;

        try {
            FileReader fileReader = new FileReader(fileName);
            BufferedReader bufferedReader = new BufferedReader(fileReader);
            
            List<String[]> aliases = new ArrayList<String[]>();
            String targetName = "";
            
            while((line = bufferedReader.readLine()) != null) {
            	
            	while (line != null && line.split(" ")[0] != null && line.split(" ")[0].equals("use")) {
            		aliases.add(new String[] {line.split(" ")[1], line.split(" ")[3]});
            		line = bufferedReader.readLine();
            	}
            	            	
            	int nTask=1;
            	while((line = bufferedReader.readLine()) != null) {
            		if(!line.equals("-")) {

            			targetName = line.split(":")[0];
            			
            			System.out.println("Starting task " + nTask);
	            		System.out.println(targetName);

            			Target target_current = new Target(targetName);
            			
            			int indice=0;
            			while(!line.split(":")[1].split(",")[indice].equals("!")) {
            				//DEPENDENCY LAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAA
            				System.out.println("Nouvelle Dependence: -" + line.split(":")[1].split(",")[indice] + "-");
            				indice++;
            			}
            			
	            		line = bufferedReader.readLine();
	            		
	            		
	            		//Echo : message
	            		
	            		
	            		line = bufferedReader.readLine();
	            		
	            		while(!line.split("\\[")[0].equals("echo")) {

	            			Class<?> clazz;
	            			List<String> attributs = new ArrayList<String>();
	            			List<String> fieldName = new ArrayList<String>();
	            			
	            			for(String[] nameDuo : aliases) {
	            				if(nameDuo[1].equals(line.split("\\[")[0])) {
	            					try {
										clazz = Class.forName(nameDuo[0]);
									
										for(int i=1;i<line.split("\"").length;i+=2) {
											attributs.add(line.split("\"")[i]);
										}
										
										fieldName.add(line.split(":")[0].split("\\[")[1]);
										
										for(int i=1;i<line.split(":").length-1;i++) {
											fieldName.add(line.split(":")[i].split(" ")[1]);
										}
										
										try {
											Task instance = (Task) clazz.newInstance();
											Field fields [] = clazz.getDeclaredFields();
											
											for(int j=0; j<fieldName.size(); j++) {
												for(int i=0; i<fields.length; i++) {
													if (fieldName.get(j).equals(fields[i].getName())) {
														fields[i].setAccessible(true);
														fields[i].set(instance, attributs.get(j));
													}
												}
											}
											target_current.addTask(instance);
										} catch (Exception e) {
											e.printStackTrace();
										}
										
									} catch (ClassNotFoundException e) {
										System.out.println("Class not found : " + nameDuo[0]);
									}
            					}
            				}
	            			line = bufferedReader.readLine();
            			}
	            		targets.add(target_current);
	            		System.out.println("Completed task " + nTask++);
            		}
            	}
            }

            bufferedReader.close();         
        }
        catch(FileNotFoundException ex) {
            ex.printStackTrace();                
        }
        catch(IOException ex) {
            ex.printStackTrace();
        }
	}
	
	public void execute() {
		System.out.println("nbTargets: " + targets.size());
		for(Target t : targets) {
			t.execute();
		}
	}
	
	public void addTarget(Target t) {
		targets.add(t);
	}
	
	public Project(String name) {
		this.name = name;
		this.targets = new ArrayList<Target>();
	}
}
