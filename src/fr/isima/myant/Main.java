package fr.isima.myant;

public class Main {

	public static void main(String[] args) {
		System.out.println("Init...");
		Project p = new Project("test");
		
		p.loadFromFile();
		System.out.println("Init Done \n");
		System.out.println("Output:");
		p.execute();
		
	}

}
