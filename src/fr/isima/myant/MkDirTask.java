package fr.isima.myant;

import java.io.File;

public class MkDirTask extends Task {
	private String dir;

	public String getDir() {
		return dir;
	}

	public void setDir(String dir) {
		this.dir = dir;
	}
	
	public MkDirTask() {}
	
	public void execute() {
		File theDir = new File(dir);

		if (!theDir.exists()) {
		    System.out.println("creating directory: " + theDir.getName());

		    try{
		        theDir.mkdir();
		    } 
		    catch(SecurityException se){
		        System.out.println("Permission Denied");
		    }
		} else {
			System.out.println("Dir already exist.");
		}
	}
}
