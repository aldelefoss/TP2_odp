package fr.isima.myant;

import java.nio.file.FileAlreadyExistsException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class CopyTask extends Task {
	private String from;
	private String to;
	
	public String getFrom() {
		return from;
	}
	public void setFrom(String from) {
		this.from = from;
	}
	public String getTo() {
		return to;
	}
	public void setTo(String to) {
		this.to = to;
	}
	
	public CopyTask() {}

	public void execute() {
		try {
			Files.copy(Paths.get(from), Paths.get(to));
		} catch (FileAlreadyExistsException e) {
			System.out.println("CopyTask: Le fichier "+ to +" existe deja.");
		} catch (Exception e) {
			
		}
	}
}
