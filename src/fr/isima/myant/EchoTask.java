package fr.isima.myant;

public class EchoTask extends Task {
	private String message;

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}
	
	public EchoTask() {
	}
	
	public EchoTask(String message) {
		this.message = message;
	}
	
	public void execute() {
		System.out.println(message);
	}
}
