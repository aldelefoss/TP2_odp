package fr.isima.myant;

import java.util.ArrayList;
import java.util.List;

public class Target {
	private String name;
	private List<Task> tasks;
	private List<Target> dependencies;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public void execute() {
		System.out.println("Target: -" + name + "-" + " with dependencies: -" + dependencies.size() + "-");
		for(Target dep : dependencies) {
			dep.execute();
		}
		for(Task task : tasks) {
			task.execute();
		}
		
	}
	
	public void addTask(Task t) {
		tasks.add(t);
	}
	
	public void addDependency(Target t) {
		dependencies.add(t);
	}
	
	public Target(String name) {
		this.name = name;
		this.tasks = new ArrayList<Task>();
		this.dependencies = new ArrayList<Target>();
	}
}
